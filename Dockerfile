FROM registry.gitlab.com/ilab-public/alpine-miniconda:latest

MAINTAINER gregoire.leroy@3e.eu

# Install python packages
ENV MPLBACKEND=agg

# create a dedicated python env using conda
ADD environment.yml .

RUN conda env create -f environment.yml && \
    echo "source activate $(head -1 environment.yml | cut -d' ' -f2)" > ~/.bashrc && \
    conda clean -a

# add env as default when running container
ENV PATH /opt/conda/envs/$(head -1 environment.yml | cut -d' ' -f2)/bin:$PATH

ENTRYPOINT []

CMD [ "/bin/bash" ]
